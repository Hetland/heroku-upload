'use strict'

/**
 * Dependencies
 * @ignore
 */

const cwd = process.cwd()
const path = require('path')
const express = require('express')
const morgan = require('morgan')

/**
 * Dotenv
 * @ignore
 */
const dotenv = require('dotenv').config()

/** 
 * Express
 * @ignore
 */
const app = express()
const { 
	PORT: port = 3000 
} = process.env

app.use(morgan('tiny'))
app.use(express.static(path.join(cwd, 'public')))

/**
 * Make main site
 * @ignore
 */
app.get('/message', (req, res) => {
	res.json({
		message: `I'm crazy, look at meeeeeee!`
	})
})

/**
 * Launch app
 * @ignore
 */
app.listen(port, () => console.log(`We're listening...on port ${port}`))



